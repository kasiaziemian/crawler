import logging

import default_settings
from crawler.app_constant import path_output, domain


class Node:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.url = url
        self.child_urls = set ()

class Tree:
    def __init__ (self, root):
        self.logger = logging.getLogger(__name__)
        self.root = root
        self.output_fd = None

    def write_sitemap(self):
        try:
            self.output_fd = open(file=default_settings.default_config[path_output], mode='w' )
            self.print_url_links(self.root)
        except (PermissionError, AttributeError) as err:
            self.logger.error ("Error: {0}, file {1} cannot be created".format (err,default_settings.default_config[path_output]))
        except Exception as err:
            self.logger.error ("Error: {0} while writing in output file: {1}".format (err, default_settings.default_config[path_output]))
            self.output_fd.close()
        else:
            print("Output: {}".format(default_settings.default_config[domain], default_settings.default_config[path_output]))
            print("Logs: {1}".format (default_settings.default_config[domain], "./logs" ))
            self.output_fd.close()

    def print_url_links (self, url_node, level=0):
        if not url_node: 
            return

        dash = ""
        for i in range(4 * level):
            dash += "."
        url_text = dash + str(url_node.url)

        self.output_fd.write(url_text + "\n")

        for child_url in url_node.child_urls:
            self.print_url_links(child_url, level + 1)
