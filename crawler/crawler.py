import logging
import queue
import re
import socket
import threading
import urllib.request
import urllib.parse

import default_settings
from .app_constant import *
from .parse_url import Node


class Opener (urllib.request.FancyURLopener):
    version = ""

class Crawler:
    def __init__ ( self ):
        self.logger = logging.getLogger(__name__)
        self.mutex = threading.Lock()
        self.parse_threads_number = default_settings.default_config[threads_number]
        self.parse_th_list = []
        self.url_instance = dict()
        self.domain_name = self.get_simple_url(self.get_domain_name(default_settings.default_config[domain]))
        self.host = urlparse(self.domain_name).netloc
        self.visited_urlnodes = set()
        self.new_urlnodes_queue = queue.Queue()
        self.urlnode_parse_root = None

    def start_url_parsing(self):
        self.config_urllib_proxy()
        self.urlnode_parse_root = self.get_create_urlnode(self.domain_name)

        self.insert_urlnodes_into_new_urls_queue(self.urlnode_parse_root)

        if not self.parse_threads_number:
            self.parse_site_urls()
            return

        for _ in range (self.parse_threads_number):
            th = threading.Thread(target=self.parse_site_urls)
            th.setDaemon(daemonic=False )
            self.parse_th_list.append(th)
            th.start()

    def release_urlparse_resources(self):
        for i in range (self.parse_threads_number):
            self.parse_th_list[i].join()

        return self.urlnode_parse_root

    def parse_site_urls(self):
        while True:
            new_url_node = self.fetch_urlnode_from_new_urls_queue()
            if not new_url_node: 
                break
            is_visited_urlnodes_updated = self.update_visited_urlnodes_if_newurlnode(new_url_node)

            if is_visited_urlnodes_updated:
                new_url = new_url_node.url  
                valid_page_child_urlnodes = self.find_valid_urlchildnodes_in_urlpage(new_url)

                if valid_page_child_urlnodes:
                    for child_urlnode in valid_page_child_urlnodes:
                        new_url_node.child_urls.add (child_urlnode)
                        self.insert_urlnodes_into_new_urls_queue(child_urlnode)

    def find_valid_urlchildnodes_in_urlpage(self, url):
        found_valid_urlnodes = set()

        try:
            opener = Opener()
            response = opener.open(url)

        except (urllib.error.HTTPError, socket.timeout, urllib.error.URLError) as err:
            self.logger.error( "error: {1}".format(url, err))
            return found_valid_urlnodes

        except Exception as err:
            self.logger.error ("error: {1}".format(url, err))
            return found_valid_urlnodes

        if response.getcode() != 200:
            self.logger.error("response code: {1}".format(url, response.getcode()))
            return found_valid_urlnodes

        html_page = str(response.read())
        pattern = '<a [^>]*href=[\'|"](.*?)[\'"].*?>'

        found_all_urls = re.findall (pattern, html_page)
        for link in found_all_urls:
            url_node = self.get_acceptable_urlnode(link)
            if url_node:  
                found_valid_urlnodes.add(url_node)

        return found_valid_urlnodes

    @staticmethod
    def config_urllib_proxy():
        authinfo = urllib.request.HTTPBasicAuthHandler()
        proxy_support = urllib.request.ProxyHandler(default_settings.default_config.get(proxy))
        opener = urllib.request.build_opener(proxy_support, authinfo)
        urllib.request.install_opener(opener)

    def update_visited_urlnodes_if_newurlnode(self, new_url_node):
        if self.is_url_already_visited(new_url_node):
             return False

        self.visited_urlnodes.add(new_url_node)
        return True

    def insert_urlnodes_into_new_urls_queue(self, url_node):
        if self.is_url_already_visited(url_node): 
            return
        url_node.url = urljoin(self.domain_name, url_node.url)
        self.new_urlnodes_queue.put (item=url_node, block=True)

    def fetch_urlnode_from_new_urls_queue(self):
        try:
            return self.new_urlnodes_queue.get (block=True, timeout=default_settings.default_config.get(timeout))
        except queue.Empty:
            return None

    @staticmethod
    def get_simple_url(url):
        if '#' in url:
            url = url[ :url.find ('#')]

        scheme, netloc, path, query, fragment = urlsplit(url)
        return urlunsplit((scheme, netloc, path, query, fragment))

    def get_acceptable_urlnode(self, url):
        url = self.get_simple_url(url)
        if not self.is_http_url(url): 
            return None

        if not self.is_internal_url(url): 
            return None

        urlnode = self.get_create_urlnode(url)
        if self.is_url_already_visited(urlnode): 
            return None

        return urlnode

    def get_create_urlnode(self, url):
        url = self.get_simple_url(url)

        self.mutex.acquire()
        urlnode = self.url_instance.get(url)
        if not urlnode:
            urlnode = Node (url)
            self.url_instance[url] = urlnode

        self.mutex.release()
        return urlnode

    @staticmethod
    def is_http_url(url):
        scheme, _, _, _, _ = urlsplit(url)

        if url and scheme in('http', 'https', ''):
            return True
        return False

    def is_internal_url(self, url):
        host = urlparse(url).netloc
        if host == self.host or host == '':
            return True
        return False

    def is_url_already_visited(self, urlnode):
        if urlnode in self.visited_urlnodes: 
            return True
        return False

    @staticmethod
    def get_domain_name(domain_name):
        parsed_uri = urlparse(domain_name)
        domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        return domain

