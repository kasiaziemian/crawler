import logging
import argparse
import default_settings
from crawler.app_constant import *


class UserConfig:
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    @staticmethod
    def set_app_config ():
        user_args = UserConfig.get_args ()
        UserConfig.verify_update_config(user_args)

#https://docs.python.org/3/library/argparse.html
    @staticmethod
    def get_args(test_args=[]):
        parser = argparse.ArgumentParser(description='Crawler')
        parser._optionals.title = "optional arguments"
        parser.add_argument('-nt', '--nthread', dest='n_thread', required=False, metavar='N',
                              default=4, type=int, help='number of threads (N>=0: default=4)')

        parser.add_argument('-l', '--log', dest='log_lvl', required=False, metavar='L',
                              default=2, type=int, help='log level (0<=L<=5: default=2)')

        parser.add_argument('-t', '--timeout', dest='timeout', required=False, metavar='T',
                              default=10, type=int, help='timeout in seconds (T>=0: default=10)')

        parser.add_argument('-f', '--file', dest='op_f_name', required=False, metavar="File_Name",
                              default="output.txt", type=str, help='name of the output file (default=output.txt): ' +
                                                                   'stored in the output directory')

        required = parser.add_argument_group('required arguments')

        required.add_argument('-d', '--domain', dest='domain_name', required=True, metavar="Domain",
                                type=str, help='name of the domain')

        if test_args:
            args = parser.parse_args(test_args)
        else:
            args = parser.parse_args()

        return args

    @staticmethod
    def verify_update_config(args):
        n_threads = args.n_thread
        if n_threads >= 0:
            default_settings.default_config[threads_number] = n_threads

        usr_log_lvl = args.log_lvl
        UserConfig.set_verify_log_level(usr_log_lvl)

        app_timeout = args.timeout
        if app_timeout < 0:
            app_timeout = 10
        default_settings.default_config[timeout] = app_timeout

        op_file_name = args.op_f_name
        default_settings.default_config[path_output] = "./output/" + op_file_name

        domain_name = args.domain_name
        default_settings.default_config[domain_name] = domain_name

    @staticmethod
    def set_verify_log_level(user_log_level):
        log_levels = {
            0: logging.NOTSET, 
            1: logging.DEBUG,
            2: logging.INFO,
            3: logging.WARNING,
            4: logging.ERROR,
            5: logging.CRITICAL,  
        }

        mapped_log_level = log_levels.get(user_log_level)
        if not mapped_log_level:  
            user_log_level = 2
            mapped_log_level = log_levels[user_log_level]
        default_settings.default_config[log_level] = user_log_level
        logging.getLogger().setLevel(mapped_log_level)
