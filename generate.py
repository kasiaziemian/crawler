
import logging.config

from crawler.crawler import Crawler
from crawler.config_app import UserConfig
from crawler.parse_url import Tree

logging.config.fileConfig(fname='.logging.conf', disable_existing_loggers=False)

UserConfig.set_app_config()

crawler = Crawler()
crawler.start_url_parsing()
urlnode_root = crawler.release_urlparse_resources()

url_parse_tree = Tree(urlnode_root)
url_parse_tree.write_sitemap()

