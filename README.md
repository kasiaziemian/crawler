# Crawler
Application crawls all the pages within a particular domain, produces a sitemap and saves domain URLs relations in output file. I used DFS algorithm for traverse the herarchical tree structure.


## Run
```
$ cd sitemap
$ python setup.py install
$ python generate.py -nt 8 -l 4 -t 5 -f stackoverflow.txt -d https://stackoverflow.com/
```

### Output 
```text
Output: ./output stackoverflow.txt
Logs: ./logs
```

### Help 
```
$ python generate.py -h
```
```
Required arguments:
  -d Domain, --domain Domain      domain name

Optional:
  -h, --help                        help message
  -nt N, --nthread N                number of threads (N>=0: default=4)
  -l L, --log L                     log level (0<=L<=5: default=2)
  -t T, --timeout T                 timeout (seconds) (T>=0: default=10)
  -f File_Name, --file File_Name    output file name (default=output.txt)
                        

```

